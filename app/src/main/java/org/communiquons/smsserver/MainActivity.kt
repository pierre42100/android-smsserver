package org.communiquons.smsserver

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import fi.iki.elonen.NanoHTTPD
import kotlinx.android.synthetic.main.activity_main.*
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private var srv: Server? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(R.style.AppTheme)

        setContentView(R.layout.activity_main)

        toggleServerStatusBtn.setOnClickListener { toggleServer() }

        if (!getKeyStorePath(this).exists())
            generateCertificate(this)

        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("auto_start", false))
            toggleServer()


        // Request required permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.SEND_SMS
                ), 100
            )
        }

        // Get current user IP address
        refreshIPAddresses()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_open_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private val srvPort: Int
        get() = Integer.valueOf(
            PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString("server_port", "8443")!!
        )

    private val enableWakeLock
        get() = PreferenceManager
            .getDefaultSharedPreferences(this)
            .getBoolean("wake_lock", true)

    private fun toggleServer() {
        if (srv != null) {
            server_status.setText(R.string.server_stopped)
            srv?.apply { stop() }
            srv = null

            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            server_status.setText(R.string.server_started)


            srv = Server(this, srvPort)
            srv?.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false)

            Toast.makeText(this, "Server started on port $srvPort", Toast.LENGTH_SHORT).show()


            // Lock awake if required
            if (enableWakeLock)
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }

        refreshIPAddresses()
    }

    private fun refreshIPAddresses() {
        val ip = ArrayList<String>()

        val interfaces: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
        while (interfaces.hasMoreElements()) {
            val iface: NetworkInterface = interfaces.nextElement()
            // filters out 127.0.0.1 and inactive interfaces
            if (iface.isLoopback || !iface.isUp) continue
            val addresses: Enumeration<InetAddress> = iface.inetAddresses
            while (addresses.hasMoreElements()) {
                val addr = addresses.nextElement()
                ip.add(addr.hostAddress)
                println(iface.displayName.toString() + " " + ip)
            }
        }

        ipAddresses.text = ip.joinToString("\n")

        srvPortLbl.text = "Server port $srvPort"
    }
}