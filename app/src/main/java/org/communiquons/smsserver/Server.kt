package org.communiquons.smsserver

import android.app.AlertDialog
import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Handler
import android.provider.ContactsContract
import android.provider.Telephony
import android.telephony.SmsManager
import android.util.Log
import android.webkit.MimeTypeMap
import com.google.i18n.phonenumbers.PhoneNumberUtil
import fi.iki.elonen.NanoHTTPD
import org.json.JSONArray
import org.json.JSONObject
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.InputStreamReader
import java.security.KeyStore
import javax.net.ssl.KeyManagerFactory


/**
 * Server
 *
 * Uses code from https://stackoverflow.com/questions/3012287/how-to-read-mms-data-in-android
 */
class Server(private val context: Context, port: Int) : NanoHTTPD(port) {

    private val MMS_ROJECTION = arrayOf(
        "_id",
        "recipient_ids",
        "date",
        "date_sent",
        "read",
        "type",
        "snippet",
        "body",
        "address",
        "thread_id",
        Telephony.MmsSms.TYPE_DISCRIMINATOR_COLUMN
    )

    private val authorizedAddresses = ArrayList<String>()

    init {
        // Load the keystore
        val keyStore = KeyStore.getInstance("PKCS12", "BC")
        val keyStoreInputStream = FileInputStream(getKeyStorePath(context))
        keyStore.load(keyStoreInputStream, KEYSTORE_PASS.toCharArray())
        keyStoreInputStream.close()

        val keyManagerFactory =
            KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
        keyManagerFactory.init(keyStore, KEYSTORE_PASS.toCharArray())
        makeSecure(makeSSLSocketFactory(keyStore, keyManagerFactory), null)
    }

    override fun serve(session: IHTTPSession): Response {

        // Ask for user permission
        if (!BuildConfig.DEBUG && !authorizedAddresses.contains(session.remoteIpAddress)) {
            val handler = Handler(context.mainLooper)

            val lock = Object()
            var accept = false

            synchronized(lock) {
                handler.post {

                    AlertDialog.Builder(context)
                        .setTitle(R.string.connect_dialog_title)
                        .setMessage("Accept connection from ${session.remoteIpAddress} ?")
                        .setNegativeButton(R.string.connect_dialog_reject) { _, _ ->
                            accept = false
                            synchronized(lock) { lock.notify() }
                        }
                        .setPositiveButton(R.string.connect_dialog_accept) { _, _ ->
                            accept = true
                            synchronized(lock) { lock.notify() }
                        }
                        .show()
                }

                lock.wait()
            }


            if (!accept)
                throw java.lang.Exception("Rejected connection!")
            else
                authorizedAddresses.add(session.remoteIpAddress)
        }

        // Assets routing
        if (session.uri == "/" || session.uri.startsWith("/assets/"))
            return serveAssets(session)

        // API routing
        return when (session.uri) {
            "/api/contacts" -> getContacts()
            "/api/sms/dump" -> dumpSMSs()
            "/api/sms/for_thread" -> threadSMSs(
                session.parameters["thread_id"]!!.first().toInt()
            )
            "/api/sms/unread" -> unreadSMSs()
            "/api/sms/threads_list" -> threadsList()
            "/api/sms/send" -> {
                session.parseBody(HashMap<String, String>())
                sendSMS(session.parameters["phone"]!!.first(), session.parameters["body"]!!.first())
            }
            "/api/mms/part" -> getMMSPart(
                session.parameters["id"]!!.first().toInt(),
                (session.parameters["mime"]?.first() ?: "application/binary")
            )
            else -> super.serve(session)
        }


    }

    /**
     * Serve assets
     */
    private fun serveAssets(session: IHTTPSession): Response {
        return try {
            val asset =
                context.assets.open("www${if (session.uri == "/") "/index.html" else session.uri}")

            var mime = MimeTypeMap
                .getSingleton()
                .getMimeTypeFromExtension(session.uri.split(".").last())

            if (session.uri.endsWith(".js"))
                mime = "application/javascript"

            newChunkedResponse(Response.Status.OK, mime, asset)
        } catch (e: Exception) {
            if (e is FileNotFoundException)
                Log.d("Server", "404 ${session.uri}")
            else
                e.printStackTrace()
            super.serve(session)
        }

    }

    /**
     * Get the list of contacts of the user
     *
     * Based on https://stackoverflow.com/a/12562234
     */
    private fun getContacts(): Response {
        val res = JSONArray()
        val cr: ContentResolver = context.contentResolver

        val cur: Cursor = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )!!

        while (cur.moveToNext()) {
            val id: String = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID))
            val name: String =
                cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))

            // Ignore contacts without phone numbers
            if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) < 1)
                continue

            val pCur: Cursor = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                arrayOf(id), null
            )!!

            while (pCur.moveToNext()) {
                val phoneNo: String = pCur.getString(
                    pCur.getColumnIndex(
                        ContactsContract.CommonDataKinds.Phone.NUMBER
                    )
                )


                val o = JSONObject()
                o.put("name", name)
                o.put("id", id)
                o.put("phone", rationalizePhoneNumber(phoneNo))
                res.put(o)
            }
            pCur.close()

        }

        cur.close()

        return newJSONResponse(res)
    }

    /**
     * Get all the SMSs of the user
     */
    private fun dumpSMSs(): Response {
        val cursor: Cursor = context.contentResolver.query(
            Uri.parse("content://mms-sms/complete-conversations"),
            MMS_ROJECTION, null, null, null
        )!!

        return parseSMSs(cursor)
    }

    /**
     * Get the messages of a specific thread
     */
    private fun threadSMSs(threadId: Int): Response {
        val cursor: Cursor = context.contentResolver.query(
            Uri.parse("content://mms-sms/complete-conversations"),
            MMS_ROJECTION, "thread_id = $threadId", null, null
        )!!

        return parseSMSs(cursor)
    }


    /**
     * Get the list of unread SMSs of the user
     */
    private fun unreadSMSs(): Response {
        val cursor: Cursor = context.contentResolver.query(
            Uri.parse("content://mms-sms/complete-conversations"),
            MMS_ROJECTION, "read = 0", null, null
        )!!

        return parseSMSs(cursor)
    }

    /**
     * Get the list of SMS threads
     */
    private fun threadsList(): Response {
        val cursor: Cursor = context.contentResolver.query(
            Uri.parse("content://mms-sms/conversations?simple=true"),
            null,
            null,
            null,
            "date DESC"
        )!!

        return parseSMSs(cursor)
    }

    /**
     * Parse the list of SMS of the user
     */
    private fun parseSMSs(cursor: Cursor): Response {
        val res = JSONArray()
        val recipientsMapping = getRecipientsList()

        if (cursor.moveToFirst()) { // must check the result to prevent exception
            do {
                res.put(extractSMS(recipientsMapping, cursor))
            } while (cursor.moveToNext())
        }

        cursor.close()

        return newJSONResponse(res)
    }

    /**
     * Extract current SMS from cursor
     */
    private fun extractSMS(recipientsMapping: HashMap<Int, String>, cursor: Cursor): JSONObject {
        /*// For dev only
        val msgDev = JSONObject()
        cursor.columnNames.forEach { cn ->
            msgDev.put(cn, cursor.getString(cursor.getColumnIndex(cn)))
        }
        return msgDev*/

        val isMMS = if (cursor.columnNames.contains(Telephony.MmsSms.TYPE_DISCRIMINATOR_COLUMN))
            cursor.getString(cursor.getColumnIndex(Telephony.MmsSms.TYPE_DISCRIMINATOR_COLUMN)) == "mms" else false

        val msg = JSONObject()

        if (cursor.columnNames.contains("recipient_ids"))
            msg.put(
                "phone",
                recipientsMapping[cursor.getInt(cursor.getColumnIndex("recipient_ids"))]
            )

        if (cursor.columnNames.contains("address")) {
            val colIndex = cursor.getColumnIndex("address")
            if (!cursor.isNull(colIndex))
                msg.put("phone", rationalizePhoneNumber(cursor.getString(colIndex)))
        }

        if (cursor.columnNames.contains("thread_id") && !msg.has("phone"))
            msg.put("phone", recipientsMapping[cursor.getInt(cursor.getColumnIndex("thread_id"))])



        if (cursor.columnNames.contains("recipient_ids"))
            msg.put("thread_id", cursor.getInt(cursor.getColumnIndex("recipient_ids")))
        if (cursor.columnNames.contains("thread_id"))
            msg.put("thread_id", cursor.getInt(cursor.getColumnIndex("thread_id")))

        msg.put("date", cursor.getLong(cursor.getColumnIndex("date")))
        msg.put("read", cursor.getInt(cursor.getColumnIndex("read")) != 0)
        msg.put("isSent", cursor.getInt(cursor.getColumnIndex("type")) == 2)

        if (cursor.columnNames.contains("snippet"))
            msg.put("body", cursor.getString(cursor.getColumnIndex("snippet")))

        if (cursor.columnNames.contains("body"))
            msg.put("body", cursor.getString(cursor.getColumnIndex("body")))

        // MMS dates are seconds based while SMS dates are milliseconds based
        if (msg.getLong("date") < 9999999999)
            msg.put("date", msg.getLong("date") * 1000)

        msg.put("isMMS", isMMS)

        // For MMS, get parts list
        if (isMMS) {

            msg.put("isSent", cursor.getString(cursor.getColumnIndex("date_sent")) == "0")

            val parts = JSONArray()
            val msgID = cursor.getInt(cursor.getColumnIndex("_id"))


            val selectionPart = "mid=$msgID"
            val uri = Uri.parse("content://mms/part")
            val cPart: Cursor? = context.contentResolver.query(
                uri, null,
                selectionPart, null, null
            )
            if (true == cPart?.moveToFirst()) {
                do {
                    val partId = cPart.getString(cPart.getColumnIndex("_id"))
                    val type = cPart.getString(cPart.getColumnIndex("ct"))

                    val obj = JSONObject()
                    obj.put("id", partId)
                    obj.put("content_type", type)

                    // Handle text parts
                    if ("text/plain" == type) {
                        val data = cPart.getString(cPart.getColumnIndex("_data"))
                        val body: String? = if (data != null) {
                            getMmsText(partId)
                        } else {
                            cPart.getString(cPart.getColumnIndex("text"))
                        }

                        obj.put("content", body)
                    }

                    parts.put(obj)
                } while (cPart.moveToNext())
            }
            cPart?.close()

            msg.put("parts", parts)
        }

        return msg
    }

    private fun getMmsText(id: String): String? {
        val partURI = Uri.parse("content://mms/part/$id")
        var inIs: InputStream? = null
        try {
            inIs = context.contentResolver.openInputStream(partURI)

            val isr = InputStreamReader(inIs, "UTF-8")
            val text = isr.readText()
            isr.close()
            return text

        } finally {
            inIs?.close()
        }
    }

    /**
     * Send a SMS
     */
    private fun sendSMS(phone: String, body: String): Response {
        Log.d("SendSMS", "phone: $phone\nbody: $body")

        val smsManager = SmsManager.getDefault()

        if (body.length < 160)
            smsManager.sendTextMessage(phone, null, body, null, null)
        else {
            val parts: ArrayList<String> = smsManager.divideMessage(body)
            smsManager.sendMultipartTextMessage(phone, null, parts, null, null)
        }

        return newFixedLengthResponse("ok")
    }

    private fun newJSONResponse(res: JSONArray): Response {
        return newFixedLengthResponse(Response.Status.OK, "application/json", res.toString())
    }

    private fun rationalizePhoneNumber(phoneNo: String): String {
        try {
            val phoneUtils = PhoneNumberUtil.getInstance()
            val phoneNumberParsed = phoneUtils.format(
                phoneUtils.parse(phoneNo, "FR"),
                PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL
            )
            return phoneNumberParsed.format(
                phoneNumberParsed,
                PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL
            )
        } catch (e: Exception) {
            Log.v("Server", "Failed to parse $phoneNo")
            return phoneNo
        }
    }

    /**
     * Get the mapping between recipients IDs and phone number
     */
    private fun getRecipientsList(): HashMap<Int, String> {
        val cursor: Cursor? = context.contentResolver.query(
            Uri.parse("content://mms-sms/canonical-addresses"),
            arrayOf("*"), null, null, null
        )

        val map = HashMap<Int, String>()

        if (cursor?.moveToFirst() == true) { // must check the result to prevent exception
            do {

                map[cursor.getInt(cursor.getColumnIndex("_id"))] =
                    rationalizePhoneNumber(cursor.getString(cursor.getColumnIndex("address")))

            } while (cursor.moveToNext())
        }
        cursor?.close()
        return map
    }

    /**
     * Get an MMS part
     */
    private fun getMMSPart(id: Int, mimeType: String): Response {
        val partURI = Uri.parse("content://mms/part/$id")
        val bIn = context.contentResolver.openInputStream(partURI)

        return newChunkedResponse(Response.Status.OK, mimeType, bIn)
    }
}