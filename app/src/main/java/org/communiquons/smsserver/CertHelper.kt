package org.communiquons.smsserver

/**
 * Functions built with the help of:
 * https://www.programcreek.com/java-api-examples/?api=org.bouncycastle.cert.X509v3CertificateBuilder
 *
 * @author Pierre Hubert
 */

import android.content.Context

import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder

import java.io.File
import java.io.FileOutputStream
import java.math.BigInteger
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.Certificate
import java.security.cert.X509Certificate
import java.util.*


fun getKeyStorePath(ctx: Context): File {
    return File(ctx.filesDir, KEYSTORE_NAME)
}

fun generateCertificate(ctx: Context) {
    val gen: KeyPairGenerator = KeyPairGenerator.getInstance("RSA")
    gen.initialize(KEY_SIZE)
    val keyPair: KeyPair = gen.generateKeyPair()

    val name = X500Name("cn=127.0.0.1")
    val subPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.public.encoded)
    val start = Date()
    val until = Calendar.getInstance().apply { set(Calendar.YEAR, KEY_EXPIRATION_YEAR) }.time
    val builder = X509v3CertificateBuilder(
        name,
        BigInteger(10, SecureRandom()),  //Choose something better for real use
        start,
        until,
        name,
        subPubKeyInfo
    )

    val signer = JcaContentSignerBuilder("SHA256WithRSA")
        .setProvider(BouncyCastleProvider())
        .build(keyPair.private)

    val holder: X509CertificateHolder = builder.build(signer)

    val cert: X509Certificate = JcaX509CertificateConverter()
        .setProvider(BouncyCastleProvider())
        .getCertificate(holder)

    val keyStore = KeyStore.getInstance("PKCS12", "BC")
    keyStore.load(null, KEYSTORE_PASS.toCharArray())
    keyStore.setKeyEntry(
        CERT_ALIAS,
        keyPair.private,
        KEYSTORE_PASS.toCharArray(),
        arrayOf<Certificate>(cert)
    )

    val file = getKeyStorePath(ctx)
    keyStore.store(FileOutputStream(file), KEYSTORE_PASS.toCharArray())
}
