package org.communiquons.smsserver

/**
 * The address where server will listen on
 */
const val SERVER_PORT = 8443

/**
 * Information about HTTPS certificate keystore
 */
const val KEYSTORE_NAME = "keystore.bks"
const val KEYSTORE_PASS = "myKeyStorePass"
const val CERT_ALIAS = "my_cert"
const val KEY_SIZE = 4096
const val KEY_EXPIRATION_YEAR = 2040