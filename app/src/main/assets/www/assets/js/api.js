/**
 * API requests
 * 
 * @author Pierre Hubert
 */

export class Contact {
    constructor(info) {
        this.id = info.id
        this.name = info.name
        this.phone = info.phone
    }
}

export class MMSPart {
    constructor(info) {
        this.id = info.id
        this.type = info.content_type
        this.content = info.content
    }

    get link() {
        return "/api/mms/part?id="+this.id+"&mime="+this.type;
    }
}

export class Message {
    constructor(info) {
        this.phone = info.phone
        this.thread_id = info.thread_id
        this.date = new Date()
        this.date.setTime(info.date)
        this.read = info.read
        this.isSent = info.isSent
        this.body = info.body
        this.isMMS = info.isMMS

        if(this.isMMS)
            /**
             * @type {MMSPart[]}
             */
            this.parts = info.parts.map(e => new MMSPart(e))
    }
}

/**
 * Get the list of contacts of the client
 * 
 * @returns {Promise<Contact[]>}
 */
export async function getContacts() {
    const contacts = await (await fetch("/api/contacts")).json();

    return contacts.map((c) => new Contact(c))
}

/**
 * Get messages threads list
 * 
 * @return {Promise<Message[]>}
 */
export async function getMessageThreads() {
    const msgs = await (await fetch("/api/sms/threads_list")).json();

    return msgs.map((c) => new Message(c))
}

/**
 * Get the messages exchanged with a specific thread
 * 
 * @return {Promise<Message[]>}
 */
export async function getMessagesForThread(thread_id) {
    const msgs = await (await fetch("/api/sms/for_thread?thread_id="+thread_id)).json();

    return msgs.map((c) => new Message(c))
}

/**
 * Send a new message
 * 
 * @param {String} phone Target phone number
 * @param {String} message The message to send
 */
export async function sendMessage(phone, message) {
    if(message.endsWith("\n"))
        message = message.substr(0, message.length - 1);

    const data = new URLSearchParams()
    data.append("phone", phone)
    data.append("body", message)

    const res = await fetch("/api/sms/send", {
        method: "POST",
        body: data
    })

    if(res.status != 200)
        throw new Error("Failed to send message!")
}