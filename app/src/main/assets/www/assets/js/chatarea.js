import { getMessagesForThread } from "./api.js";
import { getActivePhone, getActiveThreadId } from "./main.js";
import { espaceHTML, formatDate } from "./utils.js";


const messagesTarget = document.getElementById("msgs_target")

let last_active_phone = undefined;
let last_message_date = undefined;

/**
 * Refresh the list of chat messages
 */
export async function refreshChatMessages() {
    try {
        if(getActivePhone() == null) {
            console.log("No active conversation")
            return
        }

        console.log("Refresh messages for ", getActivePhone())

        const messages = await getMessagesForThread(getActiveThreadId());

        messages.sort((a, b) => a.date - b.date)

        messagesTarget.innerHTML = "";

        if(messages.length == 0)
            return;

        for(const message of messages) {
            

            if(!message.isMMS)
                applyTextMessage(message.isSent, messagesTarget, message.body, message.date)
            
            // Process MMS parts
            else
                for(const part of message.parts) {
                    switch(part.type) {

                        // MMS layout
                        case "application/smil":
                            break;

                        // Text message
                        case "text/plain":
                            applyTextMessage(message.isSent, messagesTarget, part.content, message.date)
                            break;
                        
                        // Image
                        case "image/png":
                        case "image/jpeg":
                            applyImage(message.isSent, messagesTarget, part.link, message.date);
                            break;

                        // Unknown file type
                        default:
                            applyButton(message.isSent, messagesTarget, "File (" + part.type + ")", part.link, message.date)
                            break;
                    }
                }
        }

        /* Smart scrolling */
        if(last_active_phone != getActivePhone()) {
            last_active_phone = getActivePhone()
            last_message_date = undefined;
        }

        if(last_message_date == undefined || messages[messages.length-1].date > last_message_date)
            // Scroll to latest message
            setTimeout(() => messagesTarget.scrollTop = messagesTarget.scrollHeight, 100)
        
        last_message_date = messages[messages.length-1].date
        

    } catch(e) {
        console.error(e)
        alert("Erreur lors de la récupération des messages !")
    }
}

function applyTextMessage(isSent, target, msg, date) {
    const msgEl = document.createElement("div");
    target.appendChild(msgEl)
    msgEl.classList.add(isSent ? "outgoing_msg" : "received_msg");

    const msgElInner = document.createElement("div")
    msgEl.appendChild(msgElInner)
    msgElInner.classList.add(isSent ? "sent_msg" : "received_withd_msg");
    msgElInner.innerHTML = "<p>" + espaceHTML(msg) + "</p><span class='time_date'>" + formatDate(date) + "</span>"
}

function applyButton(isSent, target, label, link, date) {
    const msgEl = document.createElement("div");
    target.appendChild(msgEl)
    msgEl.classList.add(isSent ? "outgoing_msg" : "received_msg");

    const msgElInner = document.createElement("div")
    msgEl.appendChild(msgElInner)
    msgElInner.classList.add(isSent ? "sent_msg" : "received_withd_msg");
    msgElInner.innerHTML = "<a type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">"+label+"</a> <span class='time_date'>" + formatDate(date) + "</span>"

    const a = msgElInner.querySelector("a")
    a.href = link
    a.target = "_blank"
}

function applyImage(isSent, target, link, date) {
    const msgEl = document.createElement("div");
    target.appendChild(msgEl)
    msgEl.classList.add(isSent ? "outgoing_msg" : "received_msg");

    const msgElInner = document.createElement("div")
    msgEl.appendChild(msgElInner)
    msgElInner.classList.add(isSent ? "sent_msg" : "received_withd_msg");
    msgElInner.innerHTML = "<a type=\"button\" class=\"btn btn-secondary btn-lg btn-block\"><img src=\""+link+"\" class=\"img-thumbnail\"></a> <span class='time_date'>" + formatDate(date) + "</span>"

    const a = msgElInner.querySelector("a")
    a.href = link
    a.target = "_blank"
}