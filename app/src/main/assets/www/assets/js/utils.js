/**
 * Project utilities
 * 
 * @author Pierre Hubert
 */

/**
 * Espace HTML characters "<" and ">"
 * 
 * @param {String} str Input string
 */
export function espaceHTML(str) {
    return str.replaceAll("<", "&lt;").replaceAll(">", "&gt;")
}

/**
 * Lpad
 * 
 * @param {String} str 
 * @param {String} ch 
 * @param {Number} size 
 */
export function lpad(str, ch, size) {
    str = new String(str)
    return ch.repeat((size-str.length)/ch.length) + str;
}

function twoDigits(num) {
    return lpad(num, "0", 2)
}

/**
 * Format a date to display it
 * 
 * @param {Date} date The date to format
 */
export function formatDate(date) {
    return twoDigits(date.getHours()) + ":" + twoDigits(date.getMinutes()) + " | " + twoDigits(date.getDay()) + "/" + twoDigits(date.getMonth()+1) + "/" + date.getFullYear();
}