import { Contact, getContacts, getMessageThreads, Message, sendMessage } from './api.js'
import { refreshChatMessages } from './chatarea.js';
import { espaceHTML, formatDate } from './utils.js';

const threadsList = document.getElementById("threads-list")
const sendMsgForm = document.getElementById("msgForm")
const newThreadPhoneNumberID = document.getElementById("newThreadPhoneNumberID")
const filterThreadsInput = document.getElementById("filterThreadsInput")

/**
 * @type {Array<Contact>}
 */
let contacts;

let newPhoneNumber = null

async function init() {

    // Get contacts list
    contacts = await getContacts()

    await applyMessagesThreads();
}

/**
 * Apply new list of message threads
 */
async function applyMessagesThreads() {
    const msgThreads = await getMessageThreads();
    
    let previousActiveConversation = getActiveThreadId()
    threadsList.innerHTML = "";

    let lastThreadId = Math.max.apply(null, msgThreads.map(msg => msg.thread_id))

    // Virtually create new conversation for missing contacts in the list
    for(const contact of contacts) {
        if(msgThreads.find(msg => msg.phone == contact.phone) == undefined) {
            msgThreads.push(new Message({
                thread_id: ++lastThreadId,
                phone: contact.phone,
                date: new Date(),
                read: true,
                body: ""
            }))
        }
    }

    // Virtually create new phone number
    if(newPhoneNumber != null) {
        
        const msg = msgThreads.find(msg => msg.phone == newPhoneNumber);

        if(msg == null) {
            msgThreads.unshift(new Message({
                thread_id: 0,
                phone: newPhoneNumber,
                date: new Date(),
                body: ""
            }))

            if(previousActiveConversation == null)
                previousActiveConversation = 0
        }
        
        else {
            // Auto-select thread if possible
            newPhoneNumber = null

            if(previousActiveConversation == null || previousActiveConversation == 0)
                previousActiveConversation = msg.thread_id
        }
    }

    // Get & apply the list of threads
    for(let msg of msgThreads) {
        // Try to find contact
        const contact = contacts.find((c) => c.phone == msg.phone);


        const el = document.createElement("a");
        el.className = "list-group-item list-group-item-action thread_list_tile"
        el.innerHTML = 
            "<div class=\"d-flex w-100 justify-content-between\">" +
                "<span class='thread_name'>"+(contact ? contact.name : msg.phone) + "</span>"+
                "<small>"+formatDate(msg.date)+"</small>"+
            "</div>" +
            "<small>" + (msg.body ? espaceHTML(msg.body) : "") + "</small>"

        el.setAttribute("phone", msg.phone)
        el.setAttribute("thread_id", msg.thread_id)
        threadsList.appendChild(el)

        if(msg.thread_id == previousActiveConversation)
            el.classList.add("active")

        if(!msg.read)
            el.classList.add("unread-msg")

        // Make the contact lives
        el.addEventListener("click", e => {
            e.preventDefault()

            sendMsgForm.style.display = "block";

            resetActiveThread()
            setActiveThread(msg.thread_id)

            refreshChatMessages()
        })
    }

    applyFilterThreads()
}


function createNewThread() {
    newThreadPhoneNumberID.value = ""
    $("#createThread").modal()
}

async function submitCreateNewThread() {
    if(newThreadPhoneNumberID.value.length != 14 || newThreadPhoneNumberID.value.includes("_")) {
        alert("Numéro de téléphone incomplet !")
        return;
    }
    
    resetActiveThread();
    newPhoneNumber = "+33 " + newThreadPhoneNumberID.value.substr(1)
    await applyMessagesThreads()

    $("#createThread").modal("hide")
}

function setActiveThread(thread_id) {
    document.querySelector(".thread_list_tile[thread_id=\""+thread_id+"\"]").classList.add("active")
}

function resetActiveThread() {
    const previousEl = threadsList.querySelector(".active")
    if(previousEl != null) previousEl.classList.remove("active")
}

export function getActivePhone() {
    const el = threadsList.querySelector(".active")
    return el == null ? null : el.getAttribute("phone")
}

export function getActiveThreadId() {
    const el = threadsList.querySelector(".active")
    return el == null ? null : el.getAttribute("thread_id")
}

function applyFilterThreads() {
    const filter = String(filterThreadsInput.value);

    for(const el of document.querySelectorAll(".thread_list_tile")) {
        if(
            filter.length == 0 ||
            el.querySelector(".thread_name").innerText.toLowerCase().includes(filter.toLowerCase())
        )
            el.classList.remove("display_none")
        else
            el.classList.add("display_none")
    }
}


(async () => {
    try {
        init();

        setInterval(() => refreshChatMessages(), 3000);

        setInterval(() => applyMessagesThreads(), 15000);

        // Offer to create new conversation thread
        document.getElementById("new_thread_link").addEventListener("click", () => {
            createNewThread()
        })

        document.getElementById("submitCreateThreadButton").addEventListener("click", () => {
            submitCreateNewThread()
        })

        // Make threads filtering lives
        filterThreadsInput.addEventListener("keyup", () => applyFilterThreads());

        // Make send message form lives
        const submitForm = async () => {
            let emojiArea = $("#new_msg_input")[0].emojioneArea;
            
            if(emojiArea.getText().length == 0)
                return;

            try {
                await sendMessage(getActivePhone(), emojiArea.getText())
                emojiArea.setText("")
            } catch(e) {
                console.error(e);
                alert("Failed to send message !");
            }
        }

        sendMsgForm.querySelector("button").addEventListener("click", () => submitForm())
        sendMsgForm.addEventListener("keydown", e => {
            if(e.key == "Enter") submitForm()
        })
        sendMsgForm.addEventListener("submit", (e) => {
            e.preventDefault()
            submitForm()
        })

        sendMsgForm.style.display = "none";

        // Enable emoji picker
        $(document).ready(function() {
            $("#new_msg_input").emojioneArea()
        });

        // Phone input mask
        $(newThreadPhoneNumberID).inputmask("09 99 99 99 99")

    } catch(e) {
        console.error(e)
        alert("Failed to init!")
    }
})()